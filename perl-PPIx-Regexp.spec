%global _empty_manifest_terminate_build 0

Name:           perl-PPIx-Regexp
Version:        0.088
Release:        2
Summary:        Represent a regular expression of some sort
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/PPIx-Regexp
Source0:        https://cpan.metacpan.org/authors/id/W/WY/WYANT/PPIx-Regexp-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  perl >= 0:5.006
BuildRequires:  perl-generators
BuildRequires:  perl(Carp)
BuildRequires:  perl(constant)
BuildRequires:  perl(Encode)
BuildRequires:  perl(Exporter)
BuildRequires:  perl(List::Util)
BuildRequires:  perl(Module::Build)
BuildRequires:  perl(PPI::Document) >= 1.238
BuildRequires:  perl(PPI::Dumper) >= 1.238
BuildRequires:  perl(Scalar::Util)
BuildRequires:  perl(strict)
BuildRequires:  perl(Task::Weaken)
BuildRequires:  perl(Test::More) >= 0.88
BuildRequires:  perl(warnings)
Requires:       perl(Carp)
Requires:       perl(constant)
Requires:       perl(Encode)
Requires:       perl(Exporter)
Requires:       perl(List::Util)
Requires:       perl(PPI::Document) >= 1.238
Requires:       perl(PPI::Dumper) >= 1.238
Requires:       perl(Scalar::Util)
Requires:       perl(strict)
Requires:       perl(Task::Weaken)
Requires:       perl(warnings)

%description
The purpose of the PPIx-Regexp package is to parse regular expressions in a
manner similar to the way the PPI package parses Perl. This class forms the
root of the parse tree, playing a role similar to PPI::Document.

%package help
Summary: Represent a regular expression of some sort
Provides: perl-PPIx-Regexp-doc

%description help
The purpose of the PPIx-Regexp package is to parse regular expressions in a
manner similar to the way the PPI package parses Perl. This class forms the
root of the parse tree, playing a role similar to PPI::Document.

%prep
%autosetup -n PPIx-Regexp-%{version} -p1

%build
export PERL_MM_OPT=""
%{__perl} Build.PL --installdirs=vendor
./Build

%install
./Build install --destdir=$RPM_BUILD_ROOT --create_packlist=0

%{_fixperms} $RPM_BUILD_ROOT/*

%check
./Build test

%files
%license LICENSES
%doc Changes CONTRIBUTING eg META.json README xt
%{perl_vendorlib}/*

%files help
%{_mandir}/man?/*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 0.088-2
- drop useless perl(:MODULE_COMPAT) requirement

* Sat Sep 09 2023 yaoxin <yao_xin001@hoperun.com> - 0.088-1
- Update to 0.088

* Wed Jul 06 2022 misaka00251 <misaka00251@misakanet.cn> - 0.085-1
- Init package
